
import requests
import json

def request_http(url, params=None):
    r = None
    try:
        # print(url)
        r = requests.post(url=url, json=params, verify=False)
    except Exception as e:
        print(e)
    return r


def get_mobile_extension_test( u1, u2 = None):
    _input = {
        "from": u1,
        "app_name": "MYTINPNC_ANDROID",
    }
    if u2 is not None:
        _input["to"] = u2

    url = "http://127.0.0.1:5555/api/v1/mobile/get_extension"

    print("Send request to {}\n input: {}".format(url, json.dumps(_input, indent=4)))
    
    r = request_http(url, _input)

    print("Response : {}".format(r.status_code))
    print(json.dumps(r.json(), indent=4))


def get_web_extension_test( u1, u2 = None):
    _input = {
        "from": u1,
    }
    if u2 is not None:
        _input["to"] = u2

    url = url_root + "/get_extension"

    print("Send request to {}\n input: {}".format(url, json.dumps(_input, indent=4)))
    
    r = request_http(url, _input)

    print("Response : {}".format(r.status_code))
    print(json.dumps(r.json(), indent=4))



def test_submit(username, domain):

    msg = "this is a test message"
    receiver_extension = {
        "username": username,
        "sip_domain": domain,
    }
    user_info = {
        "username": "system",
        "email": "system"
    }

    data = {
        "sender_info": user_info, # whose do this action
        'receiver_extension': receiver_extension,
        "message": msg
    }

    url = url_root + "/submit"

    print("Send request to {}\n input: {}".format(url, json.dumps(data, indent=4)))

    r = request_http(url, data)

    print("Response : {}".format(r.status_code))
    print(json.dumps(r.json(), indent=4))


def test_add_user():
    get_mobile_extension_test(u1, u2)
    get_web_extension_test(u1, u2)


if __name__ == '__main__':
    domain = "myptchat.mypt.vn";
    u1 = "nipt@fpt.com.vn"
    u2 = "anhnt232@fpt.com.vn"
    url_root = "http://localhost:5555/api/v1"
    #url_root = "https://wchat.vn/api/v1"

    test_add_user()
    test_submit("anhnt232_fpt.com.vn", domain)

    
    
