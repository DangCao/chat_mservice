
from datetime import datetime, timedelta, date

LOG_DIR_ROOT = "../../log"


XMPP_HTTP_SERVER = "http://127.0.0.1:5281"

KAMAILIO_USER_PW  = "12345678a@"
KAMAILIO_DOMAIN   = "myptchat.mypt.vn"
KAMAILIO_TCP      = "TCP"
KAMAILIO_TCP_PORT = "5062"
KAMAILIO_WSS      = "WSS"
KAMAILIO_WSS_PORT = "5067"

SUPPORT_ACC = "pnc.support"

KAMAILIO_TCP_PROXY = "{}:{}".format(KAMAILIO_DOMAIN, KAMAILIO_TCP_PORT)
KAMAILIO_WSS_PROXY = "{}:{}".format(KAMAILIO_DOMAIN, KAMAILIO_WSS_PORT)

SIP_SERVER_KAMAILIO = 1
SIP_SERVER_ONCALL   = 2

SIP_CLIENT_MOBILE = 1
SIP_CLIENT_WEB = 2


DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'



def get_current_datetime():
    return datetime.utcnow() + timedelta(hours=7)


def get_current_datetime_str():
    d = get_current_datetime()
    s = d.strftime(DATETIME_FORMAT)
    return s
